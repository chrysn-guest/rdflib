#! /usr/bin/make -f

# export DH_VERBOSE=1
# export DH_OPTIONS=-v
# export PYBUILD_VERBOSE=1
# set verbosity of nose tests if needed, by uncommenting the following
#TESTS_VERBOSITY=--verbosity=2

# uncomment to deactivate tests to make package generation quicker
#export PYBUILD_DISABLE_python2=test

export PYBUILD_NAME=rdflib

# Install (only) those into the -tools package
export PYBUILD_AFTER_INSTALL_python3=mv '{destdir}/usr/bin' 'debian/python-rdflib-tools/usr/'
# The Python 2 part is only available as a library, not as tools
export PYBUILD_AFTER_INSTALL_python2=rm -rf '{destdir}/usr/bin'

%:
ifneq "$(shell dh_listpackages | grep -- -doc)" ""
	dh $@ --with python2,python3,sphinxdoc --buildsystem=pybuild
else
	dh $@ --with python2,python3 --buildsystem=pybuild
endif

override_dh_auto_build:
ifneq "$(shell dh_listpackages | grep -- -doc)" ""
	python setup.py build_sphinx
endif

override_dh_sphinxdoc:
ifneq "$(shell dh_listpackages | grep -- -doc)" ""
	dh_sphinxdoc
endif
#override_dh_sphinxdoc:
#(test -d $(CURDIR)/debian/python-rdflib-doc && dh_sphinxdoc) || /bin/true

# Make sure pybuild doesn't prevent running tests that require network connections (test_conneg) by setting proxy env vars
# Skips the test_finalnewline as that fails with JSON-LD.
# Skips the test_roundtrip test (should skip its test_cases(('nt', 'json-ld',
#   'test/nt/keywords-04.nt'),), but don't know how to state that) as that fails
#   with JSON-LD.
override_dh_auto_test:
	#This is supposed to be working at least for Python 2, and runs 1352 tests (71 skipped):
	# PYBUILD_DISABLE_python3=test \
	# PYBUILD_TEST_ARGS=--exclude=test_conneg \
	# dh_auto_test --buildsystem=pybuild
	#But we can alternatively use upstream provided script, which runs 1415 tests (71 skipped):
	#In a pybuild-compatible way, it goes :
	# test_dawg and test_issue375 are excluded because they would use the system's rdflib in standalone interpreters
	PYBUILD_DISABLE_python3=test \
	PYBUILD_SYSTEM=custom \
        PYBUILD_TEST_ARGS="{interpreter} run_tests.py $(TESTS_VERBOSITY) --exclude=test_conneg --exclude=test_dawg --exclude=test_issue375 --exclude=test_finalnewline --exclude=test_roundtrip" \
	dh_auto_test --buildsystem=pybuild
	#Then for Python 3 : (test_dawg and test_issue375 excluded pending https://github.com/RDFLib/rdflib/issues/664)
	PYBUILD_DISABLE_python2=test \
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="./run_tests_py3.sh ${TESTS_VERBOSITY} --exclude=test_conneg --exclude=test_dawg --exclude=test_issue375 --exclude=test_finalnewline --exclude=test_roundtrip" \
	dh_auto_test --buildsystem=pybuild

override_dh_installexamples:
	dh_installexamples -X.pyc

override_dh_python2:
	dh_python2 --recommends-section=sparql --recommends-section=html

override_dh_python3:
	dh_python3 --recommends-section=sparql --recommends-section=html
